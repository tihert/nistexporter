# COPYRIGHT 2020 JULIAN PLEINES
# THIS SOFTWARE IS LICENSED UNDER MIT LICENSE
# This software is not optimized for performance rather for easier expandability and understandability (hence all these dirty string compare operations)
from __future__ import annotations
import urllib.request
import urllib.error
from typing import List, Dict
import os
import sys
import argparse
import time
from argparse import ArgumentParser

##################################### GLOBAL #####################################

temperatures: List[float] = [221.6]
fluidID: str = "C124389"
pressureHigh: float = 205.0
pressureLow: float = 1
pressureInc: float = 1

satTPressureHigh: float = 73.68
satTPressureLow: float = 5.18
satTPressureInc: float = 0.5

isoThermTemperature: float = 304.2
isoThermPressureHigh: float = 200
isoThermPressureLow: float = 74
isoThermPressureInc: float = 1

isoBarPressure: float = 201

pressureScaleFactor: float = 100000
densityScaleFactor: float = 1
cpScaleFactor: float = 1000
conductivityScaleFactor: float = 1
viscosityScaleFactor: float = 1
enthalpyScaleFactor: float = 1000
jouleThompsonScaleFactor: float = 0.00001

verboseOutput: bool = False

scaleFactors: Dict[int, float] = {0: 1, 1: pressureScaleFactor, 2: densityScaleFactor, 5: enthalpyScaleFactor,
                                  8: cpScaleFactor, 10: jouleThompsonScaleFactor, 11: viscosityScaleFactor, 12: conductivityScaleFactor, 13: 1}


class MaybeFloat:
    def __init__(self, first: bool = False, last: float = 0):
        self.first = first
        self.last = last
    
    def valid(self) -> bool:
        return self.first
    
    def value(self) -> float:
        return self.last


class BoolList:
    def __init__(self, bools: List[bool] = [False, False, False, False, False, False, False]):
        self.bools = bools

    def set(self, id: int):
        self.bools[id] = True

    def setWithNistIndex(self, id: int):
        if (id == 1):
            self.set(0)
        elif (id == 2):
            self.set(1)
        elif (id == 5):
            self.set(2)
        elif (id == 8):
            self.set(3)
        elif (id == 10):
            self.set(4)
        elif (id == 11):
            self.set(5)
        elif (id == 12):
            self.set(6)

    def reset(self, id: int):
        self.bools[id] = False

    def __str__(self):
        strObject: str = ""
        broken = False
        for boolean in self.bools:
            broken = broken or boolean
        if broken is False:
            strObject += "{BoolList - False}"
        else:
            strObject += "{BoolList - "
            for boolean in self.bools:
                if boolean:
                    strObject += "True, "
                else:
                    strObject += "False, "
            strObject += "}"

        return strObject

    def __repr__(self):
        return self.__str__()
        

class NistData:
    def __init__(self, satData: List[SaturationData], data: List[DataObject]):
        self.saturationData: List[SaturationData] = satData
        self.data: List[DataObject] = data

    def heal(self):
        if(verboseOutput):
            print("healing fluid data")
        error: bool = False
        for entry in self.data:
            error = entry.heal() or error

        if(verboseOutput):
            print("healing Saturation data")
        for i in range(len(self.saturationData)):
            entry = self.saturationData[i]
            if(entry.broken == []):
                continue
            else:
                for index in entry.broken:
                    if(i == 0):
                        entry.setByIndex(index, self.getNextPossibleSaturationValue(i + 1, index))
                    else:
                        entry.setByIndex(index, self.saturationData[i - 1].getByIndex(index))
    
    def getNextPossibleSaturationValue(self, pos: int, index: int):
        if(self.saturationData[pos].broken.count(index) == 0):
            return self.saturationData[pos].getByIndex(index)
        else:
            return self.getNextPossibleSaturationValue(pos + 1, index)


class SaturationData:
    def __init__(self, raw: List[float], broken: List[int] = []):
        self.temperature = raw[0]
        self.pressure = raw[1]
        self.liquidDesity = raw[2]
        self.liquidEnthalpy = raw[3]
        self.liquidCp = raw[4]
        self.liquidJouleThompson = raw[5]
        self.liquidViscosity = raw[6]
        self.liquidConductivity = raw[7]
        self.vapourDesity = raw[8]
        self.vapourEnthalpy = raw[9]
        self.vapourCp = raw[10]
        self.vapourJouleThompson = raw[11]
        self.vapourViscosity = raw[12]
        self.vapourConductivity = raw[13]
        self.liquidSurfaceTension: MaybeFloat = MaybeFloat(True, raw[14]) if len(raw) > 14 else MaybeFloat()
        self.broken: List[int] = broken

    def getByIndex(self, index: int) -> float:
        if(index == 0):
            return self.temperature
        elif(index == 1):
            return self.pressure
        elif (index == 2):
            return self.liquidDesity
        elif(index == 3):
            return self.liquidEnthalpy
        elif (index == 4):
            return self.liquidCp
        elif(index == 5):
            return self.liquidJouleThompson
        elif (index == 6):
            return self.liquidViscosity
        elif(index == 7):
            return self.liquidConductivity
        elif (index == 8):
            return self.vapourDesity
        elif(index == 9):
            return self.vapourEnthalpy
        elif (index == 10):
            return self.vapourCp
        elif(index == 11):
            return self.vapourJouleThompson
        elif (index == 12):
            return self.vapourViscosity
        elif(index == 13):
            return self.vapourConductivity
        elif(index == 14):
            return self.liquidSurfaceTension
        
    def setByIndex(self, index: int, value):
        if(index == 0):
            self.temperature = value
        elif(index == 1):
            self.pressure = value
        elif (index == 2):
            self.liquidDesity = value
        elif(index == 3):
            self.liquidEnthalpy = value
        elif (index == 4):
            self.liquidCp = value
        elif(index == 5):
            self.liquidJouleThompson = value
        elif (index == 6):
            self.liquidViscosity = value
        elif(index == 7):
            self.liquidConductivity = value
        elif (index == 8):
            self.vapourDesity = value
        elif(index == 9):
            self.vapourEnthalpy = value
        elif (index == 10):
            self.vapourCp = value
        elif(index == 11):
            self.vapourJouleThompson = value
        elif (index == 12):
            self.vapourViscosity = value
        elif(index == 13):
            self.vapourConductivity = value
        elif(index == 14):
            self.liquidSurfaceTension = value


class DataEntry:
    def __init__(self, raw: List[float], broken: BoolList = BoolList()):
        self.pressure = raw[0]
        self.desity = raw[1]
        self.enthalpy = raw[2]
        self.cp = raw[3]
        self.jouleThompson = raw[4]
        self.viscosity = raw[5]
        self.conductivity = raw[6]
        self.phase = raw[7]
        self.needsHealing = broken

    def __str__(self):
        return "[pressure: " + str(self.pressure) + ", desity: " + str(self.desity) + ", enthalpy: " + str(self.enthalpy) + ", cp: " + str(self.cp) + ", jouleThompson: " + str(self.jouleThompson) + ", viscosity: " + str(self.viscosity) + ", conductivity: " + str(self.conductivity) + ", phase: " + str(self.phase) + "]"


class DataObject:
    def __init__(self, temperature):
        self.temperature = temperature
        self.phaseChange = -1
        self.supercritPhaseChange = -1
        self.entries: List[DataEntry] = []

    def addEntry(self, entry: DataEntry):
        self.entries.append(entry)

    def removeLastEntry(self) -> DataEntry:
        return self.entries.pop()

    def removeEntry(self, index: int):
        self.entries.pop(index)

    def getFluidDataEntry(self, index: int) -> DataEntry:
        if (index < self.phaseChange and self.phaseChange != -1):
            return self.entries[self.phaseChange]
        else:
            return self.entries[index]

    def getVapourDataEntry(self, index: int) -> DataEntry:
        if (index >= self.phaseChange and self.phaseChange != -1):
            return self.entries[self.phaseChange - 1]
        else:
            return self.entries[index]

    def heal(self) -> bool:
        if verboseOutput:
            if(self.phaseChange != -1):
                print("Phase change from liquid to vapour at: " + str(self.phaseChange))
            else:
                print("No liquid/vapour phase change detected")
            if(self.supercritPhaseChange != -1):
                print("Phase change from vapour to supercritical at: " + str(self.supercritPhaseChange))
            else:
                print("No vapour/supercritical phase change detected")

        if verboseOutput:
            print("healing undefined values")
        for pos in range(len(self.entries[0].needsHealing.bools)):
            for index in range(len(self.entries)):
                if index < self.phaseChange:
                    if self.entries[index].needsHealing.bools[pos]:
                        try:
                            if(pos == 0):
                                self.entries[index].pressure = self.entries[index - 1].pressure
                                if verboseOutput:
                                    print("healed value for Pressure? Don't know if this breaks anything")
                            elif(pos == 1):
                                self.entries[index].desity = self.entries[index - 1].desity
                                if verboseOutput:
                                    print("healed desity value for Pressure " + str(self.entries[index].pressure) + " with <" + str(self.entries[index].desity) + ">")
                            elif(pos == 2):
                                self.entries[index].enthalpy = self.entries[index - 1].enthalpy
                                if verboseOutput:
                                    print("healed enthalpy value for Pressure " + str(self.entries[index].pressure) + " with <" + str(self.entries[index].enthalpy) + ">")
                            elif(pos == 3):
                                self.entries[index].cp = self.entries[index - 1].cp
                                if verboseOutput:
                                    print("healed cp value for Pressure " + str(self.entries[index].pressure) + " with <" + str(self.entries[index].cp) + ">")
                            elif(pos == 4):
                                self.entries[index].jouleThompson = self.entries[index - 1].jouleThompson
                                if verboseOutput:
                                    print("healed jt value for Pressure " + str(self.entries[index].pressure) + " with <" + str(self.entries[index].jouleThompson) + ">")
                            elif(pos == 5):
                                self.entries[index].viscosity = self.entries[index - 1].viscosity
                                if verboseOutput:
                                    print("healed viscosity value for Pressure " + str(self.entries[index].pressure) + " with <" + str(self.entries[index].viscosity) + ">")
                            elif(pos == 6):
                                self.entries[index].conductivity = self.entries[index - 1].conductivity
                                if verboseOutput:
                                    print("healed conductivity value for Pressure " + str(self.entries[index].pressure) + " with <" + str(self.entries[index].conductivity) + ">")
                        except IndexError:
                            print("First Element has undefined value, don't know how to fix.")
                            return False
                else:
                    break
            for index in reversed(range(len(self.entries))):
                if index >= self.phaseChange:
                    if self.entries[index].needsHealing.bools[pos]:
                        try:
                            if(pos == 0):
                                self.entries[index].pressure = self.entries[index + 1].pressure
                                if verboseOutput:
                                    print("healed value for Pressure? Don't know if this breaks anything")
                            elif(pos == 1):
                                self.entries[index].desity = self.entries[index + 1].desity
                                if verboseOutput:
                                    print("healed desity value for Pressure " + str(self.entries[index].pressure) + " with <" + str(self.entries[index].desity) + ">")
                            elif(pos == 2):
                                self.entries[index].enthalpy = self.entries[index + 1].enthalpy
                                if verboseOutput:
                                    print("healed enthalpy value for Pressure " + str(self.entries[index].pressure) + " with <" + str(self.entries[index].enthalpy) + ">")
                            elif(pos == 3):
                                self.entries[index].cp = self.entries[index + 1].cp
                                if verboseOutput:
                                    print("healed cp value for Pressure " + str(self.entries[index].pressure) + " with <" + str(self.entries[index].cp) + ">")
                            elif(pos == 4):
                                self.entries[index].jouleThompson = self.entries[index + 1].jouleThompson
                                if verboseOutput:
                                    print("healed jt value for Pressure " + str(self.entries[index].pressure) + " with <" + str(self.entries[index].jouleThompson) + ">")
                            elif(pos == 5):
                                self.entries[index].viscosity = self.entries[index + 1].viscosity
                                if verboseOutput:
                                    print("healed viscosity value for Pressure " + str(self.entries[index].pressure) + " with <" + str(self.entries[index].viscosity) + ">")
                            elif(pos == 6):
                                self.entries[index].conductivity = self.entries[index + 1].conductivity
                                if verboseOutput:
                                    print("healed conductivity value for Pressure " + str(self.entries[index].pressure) + " with <" + str(self.entries[index].conductivity) + ">")
                        except IndexError:
                            print("Last Element has undefined value, don't know how to fix.")
                            print("phaseChange: " + str(self.phaseChange) + "; index: " + str(index) + "; ")
                            return False
                else:
                    break
        if (self.phaseChange != -1):
            if verboseOutput:
                print("Deleting Phase change values with pressure: \n    " + str(self.entries[self.phaseChange - 1]) + ", \n    " + str(self.entries[self.phaseChange]))
            self.removeEntry(self.phaseChange)
            self.phaseChange -= 1
            self.removeEntry(self.phaseChange)
            if verboseOutput:
                print("New phase change at: " + str(self.phaseChange))
        if (self.supercritPhaseChange != -1):
            if verboseOutput:
                print("Deleting Phase change (supercritical) values with pressure: \n    " + str(self.entries[self.supercritPhaseChange - 1]) + ", \n    " + str(self.entries[self.supercritPhaseChange]))
            self.removeEntry(self.supercritPhaseChange)
            self.supercritPhaseChange -= 1
            self.removeEntry(self.supercritPhaseChange)
            if verboseOutput:
                print("New supercritical phase change at: " + str(self.supercritPhaseChange))
        return True

    def __str__(self):
        strObject: str = "{DataObject for " + str(self.temperature) + "K"
        for entry in self.entries:
            strObject += "\n" + str(entry)
        strObject += "}"
        return strObject

    def __repr__(self):
        return self.__str__()


def main():
    global verboseOutput, pressureHigh, pressureInc, pressureLow, fluidID

    print("Running version 1.1.1")
    parser: ArgumentParser = argparse.ArgumentParser(
        description="Downloads, parses and cleans material properties from nist to generate Mortran arrays")
    parser.add_argument("-p", "--printvariables", help="prints all usable variables in the master file", action="store_true", default=False)
    parser.add_argument("-v", "--verbose", help="activates the verbose output", action="store_true", default=False)
    parser.add_argument("-f", "--file", metavar="FILE",
                        help="set the mortan master file to use, if not specified the output will be generated in a blank file", type=str)
    parser.add_argument("-o", "--output", metavar="NAME",
                        help="sets the name for the output file, if the file already exists, the file gets overwritten (default: MasterFileName+Timestamp)", type=str, default="")
    parser.add_argument("--id", metavar="FluidID",
                        help="set the fluidID and overwrite the value specified in the master file (default: {id})".format(id=fluidID), type=str)
    parser.add_argument("--min", metavar="TEMP", help="set the minimal pressure and overwrite values specified in the master file (default: {0})".format(
        pressureLow), type=float)
    parser.add_argument("--max", metavar="TEMP", help="set the maximal pressure and overwrite the value specified in the master file (default: {0})".format(
        pressureHigh), type=float)
    parser.add_argument("--inc", metavar="TEMP", help="set the increase in pressure every step and overwrite the value specified in the master file (default: {0})".format(
        pressureInc), type=float)
    parser.add_argument("--temps", metavar="FILE",
                        help="provide a CSV with your desired temperatures, this overwrites values specified in the master file (default={0})".format(temperatures), type=str, default="none")
    args = parser.parse_args()

    startTime: float = time.time()
    timestruct = time.localtime(startTime)
    print("Staring file generation at " + time.strftime("%H:%M:%S", timestruct))

    verboseOutput = args.verbose
    currentWorkingDir: str = os.getcwd()

    # if -p/--printvariables is set print the possible variables recohnized by nistExporter in a master file
    if args.printvariables:
        printVariables()
        return

    # if a masterfile was given, start modification of that file (in copy, not inplace)
    if args.file is not None:
        try:
            masterfile = open(currentWorkingDir + "\\" + args.file)
            fileContent = masterfile.readlines()
            outputFileContent: str = ""
            continueParseVariables: bool = True
            lineNumber: int = 0
            if verboseOutput:
                print("Starting to search for Variables in the master file")
            for line in fileContent:
                if continueParseVariables:
                    continueParseVariables = parseVariables(line)
                    lineNumber += 1
                    continue
                else:
                    break
            # overwrites temperatures only if a valid csv was given
            tempEntries: List[float] = getTemperatureList(currentWorkingDir, args.temps, temperatures)
            if (args.id is not None):
                fluidID = args.id
            if (args.min is not None):
                pressureLow = args.min
            if (args.max is not None):
                pressureHigh = args.max
            if (args.inc is not None):
                pressureInc = args.inc

            lineNumber = 0 if (len(fileContent) == lineNumber) else lineNumber
            currentLineNumber = 0
            try:
                fileName: str = ""
                if args.output != "":
                    fileName = "\\" + args.output
                else:
                    fileName = "\\{master}_{time}.{extension}".format(master=args.file.split(".")[0], time=time.strftime("%H_%M_%S"), extension=args.file.split(".")[1])
                nistData: NistData = downloadData(tempEntries, fluidID, pressureLow, pressureHigh, pressureInc)
                if nistData is None:
                    print("See above error message")
                    return
                nistData.heal()
                with open(currentWorkingDir + fileName, "w") as file:
                    file.write(getNotice())
                    for line in fileContent:
                        if (currentLineNumber >= lineNumber):
                            file.write(parseMarkers(line, nistData, tempEntries))
                        else:
                            currentLineNumber += 1
                            # skipping lines above <## END_VAR>
                            continue
                    file.close()
            except (OSError, IOError):
                print("file creation failed, do you need special rights to write here or do you use forbidden characters?")
                return

        except FileNotFoundError:
            print("File not found")
            pass
    else:
        tempEntries: List[float] = getTemperatureList(currentWorkingDir, args.temps)
        if (args.id is not None):
            fluidID = args.id
        if (args.min is not None):
            pressureLow = args.min
        if (args.max is not None):
            pressureHigh = args.max
        if (args.inc is not None):
            pressureInc = args.inc
        nistData = downloadData(tempEntries, fluidID, pressureLow, pressureHigh, pressureInc)
        try:
            if nistData is None:
                print("See above error message")
                return
            nistData.heal()
            with open(currentWorkingDir + "\\{f}_{min}_to_{max}_with_{inc}_inc".format(f=fluidID, min=pressureLow, max=pressureHigh, inc=pressureInc), "w+") as file:
                for outType in range(7):
                    if outType == 0:
                        file.write("      $RHO\n")
                    elif outType == 1:
                        file.write("      $CP\n")
                    elif outType == 2:
                        file.write("      $COND\n")
                    elif outType == 3:
                        file.write("      $VISC\n")
                    elif outType == 4:
                        file.write("      $ENTH\n")
                    elif outType == 5:
                        file.write("      $JT\n")
                    elif outType == 6:
                        file.write("      $SAT\n")
                    for isVapour in range(2):
                        if isVapour == 0:
                            file.write("      $LIQUID\n")
                        else:
                            file.write("      $VAPOUR\n")

                        file.write("      FINTRP2(T, P, 1)\n")
                        if outType == 0:
                            file.write(createDataBlock("        ", nistData, tempEntries,
                                                       "DENSITY_VAPOUR" if isVapour else "DENSITY_LIQUID"))
                        elif outType == 1:
                            file.write(createDataBlock("        ", nistData, tempEntries, "CP_VAPOUR" if isVapour else "CP_LIQUID"))
                        elif outType == 2:
                            file.write(createDataBlock("        ", nistData, tempEntries,
                                                       "CONDUCTIVITY_VAPOUR" if isVapour else "CONDUCTIVITY_LIQUID"))
                        elif outType == 3:
                            file.write(createDataBlock("        ", nistData, tempEntries,
                                                       "VISCOSITY_VAPOUR" if isVapour else "VISCOSITY_LIQUID"))
                        elif outType == 4:
                            file.write(createDataBlock("        ", nistData, tempEntries,
                                                       "ENTHALPY_VAPOUR" if isVapour else "ENTHALPY_LIQUID"))
                        elif outType == 5:
                            file.write(createDataBlock("        ", nistData, tempEntries,
                                                       "JOULE_THOMPSON_VAPOUR" if isVapour else "JOULE_THOMPSON_LIQUID"))
                        elif outType == 6:
                            file.write(createDataBlock("        ", nistData, tempEntries,
                                                       "DENSITY_SAT_VAP" if isVapour else "DENSITY_SAT_LIQ"))
                                                    
        except (OSError, IOError):
            print("file not creatable, are you allowed to write here?")
            return
    endTime: float = time.time()
    timestruct = time.localtime(endTime)
    neededTime = endTime - startTime
    neededTimeStruct = time.localtime(neededTime)
    print("Ending file generation at " + time.strftime("%H:%M:%S", timestruct) + ", took " + time.strftime("%M minutes %S seconds", neededTimeStruct))


def downloadData(temperatures, fluidID, pressureLow, pressureHigh, pressureInc) -> NistData:
    # download and processs normal data
    data: List[DataObject] = []
    for t in temperatures:
        if verboseOutput:
            print("Downloading data for " + str(t) + "K")
        url = f"http://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={fluidID}&Type=IsoTherm&Digits=12&PLow={pressureLow}&PHigh={pressureHigh}&PInc={pressureInc}&T={t}&RefState=DEF&TUnit=K&PUnit=bar&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=Pa*s&STUnit=N%2Fm"
        try:
            rawdata: bytes = urllib.request.urlopen(url).read()
        except urllib.error.URLError:
            print("URL not reachable, check for the error:")
            print(url)
            return None
        try:
            lines: List[str] = rawdata.decode("UTF-8").splitlines()
        except UnicodeDecodeError:
            print("response not parsable, check yourself:")
            print(rawdata)
            return None
        dataObject: DataObject = DataObject(t)
        lastPhase: int = -1
        counter: int = -1
        for line in lines[1:]:
            counter += 1
            entries = line.split("\t")
            datapoint: List[float] = []
            # broken: BoolList = BoolList()
            broken: List[bool] = [False, False, False, False, False, False, False]
            for i in [1, 2, 5, 8, 10, 11, 12, 13]:
                try:
                    datapoint.append(float(entries[i]) * scaleFactors[i])
                except ValueError:
                    if(entries[i].lower() == "undefined" and i != 13):
                        if verboseOutput:
                            print("undefined value for " + indexToType(nistIndexToIndex(i)) + "- (index: " + str(nistIndexToIndex(i)) + ", nistIndex: " + str(i) + ")")
                        datapoint.append(0)
                        broken[nistIndexToIndex(i)] = True
                    elif(entries[i].lower() == "undefined" and i == 13):
                        print("Phase undefined, Panic! If this happens, check you inputs. I these are right, contact me to discuss a solution. (https://gitlab.com/0xhexdec/nistexporter)")
                        if verboseOutput:
                            print(url)
                        return None
                    elif(entries[i] == "vapor"):
                        datapoint.append(0)
                        lastPhase = 0
                    elif (entries[i] == "liquid" and lastPhase == 0):
                        datapoint.append(1)
                        dataObject.phaseChange = counter
                        lastPhase = 1
                    elif (entries[i] == "liquid" and (lastPhase == 1 or lastPhase == -1)):
                        datapoint.append(1)
                        lastPhase = 1
                    elif (entries[i] == "supercritical" and (lastPhase == 1 or lastPhase == 0)):
                        datapoint.append(2)
                        dataObject.supercritPhaseChange = counter
                        lastPhase = 2
                    elif (entries[i] == "supercritical" and lastPhase == 2):
                        datapoint.append(2)
                    else:
                        print("Conversion failed due to non parsable value <" + entries[i] + ">")
                        if verboseOutput:
                            print(url)
                        return None
            dataObject.addEntry(DataEntry(datapoint, BoolList(broken)))
        data.append(dataObject)

    # download and process saturation data
    satDat: List[SaturationData] = []
    if verboseOutput:
        print("Downloading SatT data")
    url = f"https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={fluidID}&Type=SatT&Digits=5&PLow={satTPressureLow}&PHigh={satTPressureHigh}&PInc={satTPressureInc}&RefState=DEF&TUnit=K&PUnit=bar&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=Pa*s&STUnit=N%2Fm"
    try:
        rawdata: bytes = urllib.request.urlopen(url).read()
    except urllib.error.URLError:
        print("URL not reachable, check for the error:")
        print(url)
        return None
    try:
        lines: List[str] = rawdata.decode("UTF-8").splitlines()
    except UnicodeDecodeError:
        print("response not parsable, check yourself:")
        print(rawdata)
        return None
    for line in lines[1:]:
        entries = line.split("\t")
        datapoint: List[float] = []
        broken: List[int] = []
        index: int = -1
        for i in [0, 1, 2, 5, 8, 10, 11, 12, 14, 17, 20, 22, 23, 24, 13]:
            index += 1
            try:
                datapoint.append(float(entries[i]) * (scaleFactors[i] if i < 14 else scaleFactors[i - 12]))
            except ValueError:
                if (entries[i] == "undefined"):
                    datapoint.append(-1)
                    broken.append(index)
                    continue
                else:
                    print("Conversion failed due to non parsable value <" + entries[i] + ">")
                    print(url)
                    return None
        satDat.append(SaturationData(datapoint, broken))
    # download and process isothermal data
    if verboseOutput:
        print("Downloading IsoTherm data")
    url = f"https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&ID={fluidID}&Type=IsoTherm&Digits=5&PLow={isoThermPressureLow}&PHigh={isoThermPressureHigh}&PInc={isoThermPressureInc}&T={isoThermTemperature}&RefState=DEF&TUnit=K&PUnit=bar&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=Pa*s&STUnit=N%2Fm"
    try:
        rawdata: bytes = urllib.request.urlopen(url).read()
    except urllib.error.URLError:
        print("URL not reachable, check for the error:")
        print(url)
        return None
    try:
        lines: List[str] = rawdata.decode("UTF-8").splitlines()
    except UnicodeDecodeError:
        print("response not parsable, check yourself:")
        print(rawdata)
        return None
    for line in lines[1:]:
        entries = line.split("\t")
        datapoint: List[float] = []
        broken: List[int] = []
        index: int = -1
        for i in [0, 1, 2, 5, 8, 10, 11, 12, 2, 5, 8, 10, 11, 12]:
            index += 1
            try:
                datapoint.append(float(entries[i]) * scaleFactors[i])
            except ValueError:
                if (entries[i].lower == "undefined"):
                    datapoint.append(-1)
                    broken.append(index)
                    continue
                print("Conversion failed due to non parsable value <" + entries[i] + ">")
                return None
        satDat.append(SaturationData(datapoint, broken))
    # download and process isobaric data
    if verboseOutput:
        print("Downloading IsoBar data")
    url = f"https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={fluidID}&Type=IsoBar&Digits=5&P={isoBarPressure}&THigh={temperatures[-1]}&TLow={temperatures[-1]}&TInc=1&RefState=DEF&TUnit=K&PUnit=bar&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=Pa*s&STUnit=N%2Fm"
    print(url)
    try:
        rawdata: bytes = urllib.request.urlopen(url).read()
    except urllib.error.URLError:
        print("URL not reachable, check for the error:")
        print(url)
        return None
    try:
        lines: List[str] = rawdata.decode("UTF-8").splitlines()
    except UnicodeDecodeError:
        print("response not parsable, check yourself:")
        print(rawdata)
        return None
    for line in lines[1:]:
        entries = line.split("\t")
        datapoint: List[float] = []
        broken: List[int] = []
        index: int = -1
        for i in [0, 1, 2, 5, 8, 10, 11, 12, 2, 5, 8, 10, 11, 12]:
            index += 1
            try:
                datapoint.append(float(entries[i]) * scaleFactors[i])
            except ValueError:
                if (entries[i].lower == "undefined"):
                    datapoint.append(-1)
                    broken.append(index)
                    continue
                print("Conversion failed due to non parsable value <" + entries[i] + ">")
                return None
        satDat.append(SaturationData(datapoint, broken))

    return NistData(satDat, data)


def printVariables() -> None:
    """
    Prints all possible variables that nistExporter recognizes in the master file to the standart out
    """
    print("\nNistExporter searches for lines starting with '##', hence every keyword must start with this sequence.")
    print("The line must be empty (except spaces) up to the '##' and the keyword must follow in the same line\n")
    print("Variables are used like:")
    print("## VARIABLE=VALUE")
    print("Markers are used like:")
    print("## MARKER")
    print("\nKeyword                      | Type       | Description:")
    print("--------------------------------------------------------------------------------------------------------------------------------------")
    print("FLUID_ID                     | VARIABLE   | Sets the FluidID")
    print("PRESSURE_HIGH                | VARIABLE   | Sets the maximum pressure for the nist request")
    print("PRESSURE_LOW                 | VARIABLE   | Sets the minimum pressure for the nist request")
    print("PRESSURE_INC                 | VARIABLE   | Sets the increment between pressure points for the nist request")
    print("TEMPERATURES_LIST            | VARIABLE   | Sets the temperature points for the nist request, expects values as CSV e.g. =1.5,2.6,3.7")
    print("--------------------------------------------------------------------------------------------------------------------------------------")
    print("PRESSURE_SCALE_FACTOR        | VARIABLE   | Sets the scale factor for the pressure values")
    print("DENSITY_SCALE_FACTOR         | VARIABLE   | Sets the scale factor for the density values")
    print("CP_SCALE_FACTOR              | VARIABLE   | Sets the scale factor for the CP values")
    print("CONDUCTIVITY_SCALE_FACTOR    | VARIABLE   | Sets the scale factor for the conductivity values")
    print("ENTHALPY_SCALE_FACTOR        | VARIABLE   | Sets the scale factor for the enthalpy values")
    print("JOULE_THOMPSON_SCALE_FACTOR  | VARIABLE   | Sets the scale factor for the jouleThompson values")
    print("--------------------------------------------------------------------------------------------------------------------------------------")
    print("SAT_T_PRESSURE_HIGH          | VARIABLE   | Sets the maximum pressure for the saturation nist request")
    print("SAT_T_PRESSURE_LOW           | VARIABLE   | Sets the minimum pressure for the saturation nist request")
    print("SAT_T_PRESSURE_INC           | VARIABLE   | Sets the increment pressure for the saturation nist request")
    print("--------------------------------------------------------------------------------------------------------------------------------------")
    print("ISO_THERM_TEMPERATURE        | VARIABLE   | Sets the temperature for the isothermal nist request")
    print("ISO_THERM_PRESSURE_HIGH      | VARIABLE   | Sets the maximum pressure for the isothermal nist request")
    print("ISO_THERM_PRESSURE_LOW       | VARIABLE   | Sets the minimum pressure for the isothermal nist request")
    print("ISO_THERM_PRESSURE_INC       | VARIABLE   | Sets the increment pressure for the isothermal nist request")
    print("--------------------------------------------------------------------------------------------------------------------------------------")
    print("DENSITY_SAT_LIQ              | MARKER     | Gets replaced by the pressure/density list from saturation chart for liquid phase")
    print("DENSITY_SAT_VAP              | MARKER     | Gets replaced by the pressure/density list from saturation chart for vapour phase")
    print("DENSITY_LIQUID               | MARKER     | Gets replaced by the temperature/pressure/density list for liquid phase")
    print("DENSITY_VAPOUR               | MARKER     | Gets replaced by the temperature/pressure/density list for vapour phase")
    print("--------------------------------------------------------------------------------------------------------------------------------------")
    print("CP_SAT_LIQ                   | MARKER     | Gets replaced by the pressure/cp list from saturation chart for liquid phase")
    print("CP_SAT_VAP                   | MARKER     | Gets replaced by the pressure/cp list from saturation chart for vapour phase")
    print("CP_LIQUID                    | MARKER     | Gets replaced by the temperature/pressure/cp list for liquid phase")
    print("CP_VAPOUR                    | MARKER     | Gets replaced by the temperature/pressure/cp list for vapour phase")
    print("--------------------------------------------------------------------------------------------------------------------------------------")
    print("CONDUCTIVITY_SAT_LIQ         | MARKER     | Gets replaced by the pressure/conductivity list from saturation chart for liquid phase")
    print("CONDUCTIVITY_SAT_VAP         | MARKER     | Gets replaced by the pressure/conductivity list from saturation chart for vapour phase")
    print("CONDUCTIVITY_LIQUID          | MARKER     | Gets replaced by the temperature/pressure/conductivity list for liquid phase")
    print("CONDUCTIVITY_VAPOUR          | MARKER     | Gets replaced by the temperature/pressure/conductivity list for vapour phase")
    print("--------------------------------------------------------------------------------------------------------------------------------------")
    print("VISCOSITY_SAT_LIQ            | MARKER     | Gets replaced by the pressure/viscosity list from saturation chart for liquid phase")
    print("VISCOSITY_SAT_VAP            | MARKER     | Gets replaced by the pressure/viscosity list from saturation chart for vapour phase")
    print("VISCOSITY_LIQUID             | MARKER     | Gets replaced by the temperature/pressure/viscosity list for liquid phase")
    print("VISCOSITY_VAPOUR             | MARKER     | Gets replaced by the temperature/pressure/viscosity list for vapour phase")
    print("--------------------------------------------------------------------------------------------------------------------------------------")
    print("ENTHALPY_SAT_LIQ             | MARKER     | Gets replaced by the pressure/enthalpy list from saturation chart for liquid phase")
    print("ENTHALPY_SAT_VAP             | MARKER     | Gets replaced by the pressure/enthalpy list from saturation chart for vapour phase")
    print("ENTHALPY_LIQUID              | MARKER     | Gets replaced by the temperature/pressure/enthalpy list for liquid phase")
    print("ENTHALPY_VAPOUR              | MARKER     | Gets replaced by the temperature/pressure/enthalpy list for vapour phase")
    print("--------------------------------------------------------------------------------------------------------------------------------------")
    print("JOULE_THOMPSON_SAT_LIQ       | MARKER     | Gets replaced by the pressure/joule-thompson list from saturation chart for liquid phase")
    print("JOULE_THOMPSON_SAT_VAP       | MARKER     | Gets replaced by the pressure/joule-thompson list from saturation chart for vapour phase")
    print("JOULE_THOMPSON_LIQUID        | MARKER     | Gets replaced by the temperature/pressure/joule-thompson list for liquid phase")
    print("JOULE_THOMPSON_VAPOUR        | MARKER     | Gets replaced by the temperature/pressure/joule-thompson list for vapour phase")
    print("--------------------------------------------------------------------------------------------------------------------------------------")
    print("SURFACE_TENSION_SAT_LIQ      | MARKER     | Gets replaced by the pressure/surface tension list from saturation chart for liquid phase")
    print("TEMPERATURE                  | MARKER     | Gets replaced by the pressure/temperature list from saturation chart for liquid phase")
    print("PRESSURE                     | MARKER     | Gets replaced by the temperature/pressure list from saturation chart for liquid phase")
    print("--------------------------------------------------------------------------------------------------------------------------------------")
    print("END_VAR                      | MARKER     | Ends the section for the variable section. Optional, but speeds up the file parsing")


def getTemperatureList(currentWorkingDir: str, tempsCVS: str, fallbackTemperatures: List[float] = temperatures) -> List[float]:
    """
    Parses a CSV, expecting to hold a list of temperatures
    """
    if (tempsCVS == "none"):
        return fallbackTemperatures
    try:
        file = open(currentWorkingDir + "\\" + tempsCVS)
        csv = file.readlines()
        csvClean = ""
        for line in csv:
            csvClean += line
        entries = csvClean.replace(";", ",").replace(" ", "").replace("\t", "").replace("\n", "").replace("\r", "").split(",")
        tempEntries: List[float] = []
        for entry in entries:
            try:
                tempEntries.append(float(entry))
            except ValueError:
                print("<{0}> not convertable, trying to heal it...".format(entry))
                num = ""
                point: bool = False
                for char in entry:
                    if (char.isdigit()):
                        num += char
                    elif (char == "." and not point):
                        num += "."
                        point = True
                try:
                    tempEntries.append(float(num))
                    print("success! value was <{0}>".format(num))
                    continue
                except ValueError:
                    pass
                print("File conversion not posssible, <{0}> not convertable".format(entry))
                print("using default values")
                return fallbackTemperatures
        return tempEntries
    except FileNotFoundError:
        print("file not found, using default values")
        return fallbackTemperatures


def parseVariables(line: str) -> bool:
    """
    Parses the nistExporter varibales and populates the global variables with the parsed values
    """
    global fluidID, pressureHigh, pressureLow, pressureInc, temperatures, pressureScaleFactor, densityScaleFactor, cpScaleFactor, conductivityScaleFactor, enthalpyScaleFactor, jouleThompsonScaleFactor, satTPressureHigh, satTPressureLow, satTPressureInc, isoThermTemperature, isoThermPressureHigh, isoThermPressureLow, isoThermPressureInc, isoBarPressure

    # sanitize string
    cleanLine = line.replace(" ", "").replace("\t", "").replace("\n", "").replace("\r", "")
    if not cleanLine.startswith("##"):
        if verboseOutput:
            print("Skipping line")
        return True
    cleanLine = cleanLine.replace("##", "", 1)
    # thank you python for not having switch/case...
    try:
        if (cleanLine.startswith("END_VAR")):
            return False
        elif (cleanLine.startswith("FLUID_ID")):
            fluidID = getVariableValue(cleanLine, "FLUID_ID")
            if verboseOutput:
                print("FLUID_ID: {}".format(fluidID))
        elif (cleanLine.startswith("PRESSURE_HIGH")):
            pressureHigh = float(getVariableValue(cleanLine, "PRESSURE_HIGH"))
            if verboseOutput:
                print("PRESSURE_HIGH: {}".format(pressureHigh))
        elif (cleanLine.startswith("PRESSURE_LOW")):
            pressureLow = float(getVariableValue(cleanLine, "PRESSURE_LOW"))
            if verboseOutput:
                print("PRESSURE_LOW: {}".format(pressureLow))
        elif (cleanLine.startswith("PRESSURE_INC")):
            pressureInc = float(getVariableValue(cleanLine, "PRESSURE_INC"))
            if verboseOutput:
                print("PRESSURE_INC: {}".format(pressureInc))
        elif (cleanLine.startswith("PRESSURE_SCALE_FACTOR")):
            pressureScaleFactor = float(getVariableValue(cleanLine, "PRESSURE_SCALE_FACTOR"))
            if verboseOutput:
                print("PRESSURE_SCALE_FACTOR: {}".format(pressureScaleFactor))
        elif (cleanLine.startswith("DENSITY_SCALE_FACTOR")):
            densityScaleFactor = float(getVariableValue(cleanLine, "DENSITY_SCALE_FACTOR"))
            if verboseOutput:
                print("DENSITY_SCALE_FACTOR: {}".format(densityScaleFactor))
        elif (cleanLine.startswith("CP_SCALE_FACTOR")):
            cpScaleFactor = float(getVariableValue(cleanLine, "CP_SCALE_FACTOR"))
            if verboseOutput:
                print("CP_SCALE_FACTOR: {}".format(cpScaleFactor))
        elif (cleanLine.startswith("CONDUCTIVITY_SCALE_FACTOR")):
            conductivityScaleFactor = float(getVariableValue(cleanLine, "CONDUCTIVITY_SCALE_FACTOR"))
            if verboseOutput:
                print("CONDUCTIVITY_SCALE_FACTOR: {}".format(conductivityScaleFactor))
        elif (cleanLine.startswith("ENTHALPY_SCALE_FACTOR")):
            enthalpyScaleFactor = float(getVariableValue(cleanLine, "ENTHALPY_SCALE_FACTOR"))
            if verboseOutput:
                print("ENTHALPY_SCALE_FACTOR: {}".format(enthalpyScaleFactor))
        elif (cleanLine.startswith("JOULE_THOMPSON_SCALE_FACTOR")):
            jouleThompsonScaleFactor = float(getVariableValue(cleanLine, "JOULE_THOMPSON_SCALE_FACTOR"))
            if verboseOutput:
                print("JOULE_THOMPSON_SCALE_FACTOR: {}".format(jouleThompsonScaleFactor))
        elif (cleanLine.startswith("SAT_T_PRESSURE_HIGH")):
            satTPressureHigh = float(getVariableValue(cleanLine, "SAT_T_PRESSURE_HIGH"))
            if verboseOutput:
                print("SAT_T_PRESSURE_HIGH: {}".format(satTPressureHigh))
        elif (cleanLine.startswith("SAT_T_PRESSURE_LOW")):
            satTPressureLow = float(getVariableValue(cleanLine, "SAT_T_PRESSURE_LOW"))
            if verboseOutput:
                print("SAT_T_PRESSURE_LOW: {}".format(satTPressureLow))
        elif (cleanLine.startswith("SAT_T_PRESSURE_INC")):
            satTPressureInc = float(getVariableValue(cleanLine, "SAT_T_PRESSURE_INC"))
            if verboseOutput:
                print("SAT_T_PRESSURE_INC: {}".format(satTPressureInc))
        elif (cleanLine.startswith("ISO_THERM_TEMPERATURE")):
            isoThermTemperature = float(getVariableValue(cleanLine, "ISO_THERM_TEMPERATURE"))
            if verboseOutput:
                print("ISO_THERM_TEMPERATURE: {}".format(isoThermTemperature))
        elif (cleanLine.startswith("ISO_THERM_PRESSURE_HIGH")):
            isoThermPressureHigh = float(getVariableValue(cleanLine, "ISO_THERM_PRESSURE_HIGH"))
            if verboseOutput:
                print("ISO_THERM_PRESSURE_HIGH: {}".format(isoThermPressureHigh))
        elif (cleanLine.startswith("ISO_THERM_PRESSURE_LOW")):
            isoThermPressureLow = float(getVariableValue(cleanLine, "ISO_THERM_PRESSURE_LOW"))
            if verboseOutput:
                print("ISO_THERM_PRESSURE_LOW: {}".format(isoThermPressureLow))
        elif (cleanLine.startswith("ISO_THERM_PRESSURE_INC")):
            isoThermPressureInc = float(getVariableValue(cleanLine, "ISO_THERM_PRESSURE_INC"))
            if verboseOutput:
                print("ISO_THERM_PRESSURE_INC: {}".format(isoThermPressureInc))
        elif (cleanLine.startswith("ISO_BAR_PRESSURE")):
            isoBarPressure = float(getVariableValue(cleanLine, "ISO_BAR_PRESSURE"))
            if verboseOutput:
                print("ISO_BAR_PRESSURE: {}".format(isoBarPressure))
        elif (cleanLine.startswith("TEMPERATURES_LIST")):
            temperatures = getFloatList(getVariableValue(cleanLine, "TEMPERATURES_LIST"))
            if verboseOutput:
                print("TEMPERATURES_LIST: {}".format(temperatures))
        else:
            if verboseOutput:
                print("Skipping line, no Variable found")
        return True
    except ValueError:
        print("Conversion failed! Couldn't convert: {}".format(cleanLine))
        return False
    except:
        print("Unexpected error:", sys.exc_info()[0])


def getVariableValue(line: str, keyword: str) -> str:
    cleanedLine = line.replace(keyword + "=", "")
    return cleanedLine


def getFloatList(line: str) -> List[float]:
    strList: List[str] = line.split(",")
    floatList: List[float] = []
    for entry in strList:
        try:
            floatList.append(float(entry))
        except ValueError:
            print("Conversion Error while parsing the temperaturelist: \"{}\" couldn't be converted".format(entry))
            return []
    return floatList


def parseMarkers(line: str, nistData: NistData, tempEntries: List[float]) -> str:
    # sanitize line
    cleanLine: str = line.replace(" ", "").replace("\t", "").replace("\n", "").replace("\r", "")
    if not cleanLine.startswith("##"):
        return line
    else:
        cleanLine = cleanLine.replace("##", "", 1)
        if isMarker(cleanLine):
            prefix = line.split("#")[0]
            return createDataBlock(prefix, nistData, tempEntries, cleanLine)
        else:
            return line


def createDataBlock(linePrefix: str, nistData: NistData, tempEntries: List[float], marker: str) -> str:
    block: str = ""
    if marker.find("_SAT_") != -1 or marker == "TEMPERATURE" or marker == "PRESSURE":
        satData: List[SaturationData] = nistData.saturationData
        for i in range(len(satData)):
            if (marker.startswith("DENSITY_SAT_LIQ")):
                block += linePrefix + str(format(satData[i].pressure, '.4E')) + ", " + str(format(satData[i].liquidDesity, '.4E'))
            elif (marker.startswith("DENSITY_SAT_VAP")):
                block += linePrefix + str(format(satData[i].pressure, '.4E')) + ", " + str(format(satData[i].vapourDesity, '.4E'))
            elif (marker.startswith("CP_SAT_LIQ")):
                block += linePrefix + str(format(satData[i].pressure, '.4E')) + ", " + str(format(satData[i].liquidCp, '.4E'))
            elif (marker.startswith("CP_SAT_VAP")):
                block += linePrefix + str(format(satData[i].pressure, '.4E')) + ", " + str(format(satData[i].vapourCp, '.4E'))
            elif (marker.startswith("CONDUCTIVITY_SAT_LIQ")):
                block += linePrefix + str(format(satData[i].pressure, '.4E')) + ", " + str(format(satData[i].liquidConductivity, '.4E'))
            elif (marker.startswith("CONDUCTIVITY_SAT_VAP")):
                block += linePrefix + str(format(satData[i].pressure, '.4E')) + ", " + str(format(satData[i].vapourConductivity, '.4E'))
            elif (marker.startswith("VISCOSITY_SAT_LIQ")):
                block += linePrefix + str(format(satData[i].pressure, '.4E')) + ", " + str(format(satData[i].liquidViscosity, '.4E'))
            elif (marker.startswith("VISCOSITY_SAT_VAP")):
                block += linePrefix + str(format(satData[i].pressure, '.4E')) + ", " + str(format(satData[i].vapourViscosity, '.4E'))
            elif (marker.startswith("ENTHALPY_SAT_LIQ")):
                block += linePrefix + str(format(satData[i].pressure, '.4E')) + ", " + str(format(satData[i].liquidEnthalpy, '.4E'))
            elif (marker.startswith("ENTHALPY_SAT_VAP")):
                block += linePrefix + str(format(satData[i].pressure, '.4E')) + ", " + str(format(satData[i].vapourEnthalpy, '.4E'))
            elif (marker.startswith("JOULE_THOMPSON_SAT_LIQ")):
                block += linePrefix + str(format(satData[i].pressure, '.4E')) + ", " + str(format(satData[i].liquidJouleThompson, '.4E'))
            elif (marker.startswith("JOULE_THOMPSON_SAT_VAP")):
                block += linePrefix + str(format(satData[i].pressure, '.4E')) + ", " + str(format(satData[i].vapourJouleThompson, '.4E'))
            elif (marker.startswith("TEMPERATURE")):
                block += linePrefix + str(format(satData[i].pressure, '.4E')) + ", " + str(format(satData[i].temperature, '.4E'))
            elif (marker.startswith("PRESSURE")):
                block += linePrefix + str(format(satData[i].temperature, '.4E')) + ", " + str(format(satData[i].pressure, '.4E'))
            elif (marker.startswith("SURFACE_TENSION_SAT_LIQ")):
                if(satData[i].liquidSurfaceTension.first):
                    if i != 0:
                        block += ",\n"
                    block += linePrefix + str(format(satData[i].pressure, '.4E')) + ", " + str(format(satData[i].liquidSurfaceTension.last, '.4E'))
                else:
                    continue
            else:
                block += "UNKNOWN"

            if (i != (len(satData) - 1) and not marker.startswith("SURFACE_TENSION_SAT_LIQ")):
                block += ",\n"
        block += ";\n"

    else:
        data: List[DataObject] = nistData.data
        block += linePrefix + "T= "
        for i in range(len(data)):
            block += "{:.2f}, ".format(tempEntries[i])
        for i in range(len(data[0].entries)):
            block += "\n" + linePrefix + "P= " + str(format(data[0].entries[i].pressure, '.4E') + ", ")
            for j in range(len(data)):
                if (marker.startswith("DENSITY_LIQUID")):
                    block += str(format(data[j].getFluidDataEntry(i).desity, '.4E'))
                elif (marker.startswith("DENSITY_VAPOUR")):
                    block += str(format(data[j].getVapourDataEntry(i).desity, '.4E'))
                elif (marker.startswith("CP_LIQUID")):
                    block += str(format(data[j].getFluidDataEntry(i).cp, '.4E'))
                elif (marker.startswith("CP_VAPOUR")):
                    block += str(format(data[j].getVapourDataEntry(i).cp, '.4E'))
                elif (marker.startswith("CONDUCTIVITY_LIQUID")):
                    block += str(format(data[j].getFluidDataEntry(i).conductivity, '.4E'))
                elif (marker.startswith("CONDUCTIVITY_VAPOUR")):
                    block += str(format(data[j].getVapourDataEntry(i).conductivity, '.4E'))
                elif (marker.startswith("VISCOSITY_LIQUID")):
                    block += str(format(data[j].getFluidDataEntry(i).viscosity, '.4E'))
                elif (marker.startswith("VISCOSITY_VAPOUR")):
                    block += str(format(data[j].getVapourDataEntry(i).viscosity, '.4E'))
                elif (marker.startswith("ENTHALPY_LIQUID")):
                    block += str(format(data[j].getFluidDataEntry(i).enthalpy, '.4E'))
                elif (marker.startswith("ENTHALPY_VAPOUR")):
                    block += str(format(data[j].getVapourDataEntry(i).enthalpy, '.4E'))
                elif (marker.startswith("JOULE_THOMPSON_LIQUID")):
                    block += str(format(data[j].getFluidDataEntry(i).jouleThompson, '.4E'))
                elif (marker.startswith("JOULE_THOMPSON_VAPOUR")):
                    block += str(format(data[j].getVapourDataEntry(i).jouleThompson, '.4E'))
                else:
                    block += "UNKNOWN"

                if (i == len(data[0].entries) - 1 and j == len(data) - 1):
                    block += ";\n"
                else:
                    block += ", "
    return block


def isMarker(marker: str) -> bool:
    if marker == "DENSITY_SAT_LIQ" or marker == "DENSITY_SAT_VAP" or marker == "DENSITY_LIQUID" or marker == "DENSITY_VAPOUR" or marker == "CP_SAT_LIQ" or marker == "CP_SAT_VAP" or marker == "CP_LIQUID" or marker == "CP_VAPOUR" or marker == "CONDUCTIVITY_SAT_LIQ" or marker == "CONDUCTIVITY_SAT_VAP" or marker == "CONDUCTIVITY_LIQUID" or marker == "CONDUCTIVITY_VAPOUR" or marker == "VISCOSITY_SAT_LIQ" or marker == "VISCOSITY_SAT_VAP" or marker == "VISCOSITY_LIQUID" or marker == "VISCOSITY_VAPOUR" or marker == "ENTHALPY_SAT_LIQ" or marker == "ENTHALPY_SAT_VAP" or marker == "ENTHALPY_LIQUID" or marker == "ENTHALPY_VAPOUR" or marker == "JOULE_THOMPSON_SAT_LIQ" or marker == "JOULE_THOMPSON_SAT_VAP" or marker == "JOULE_THOMPSON_LIQUID" or marker == "JOULE_THOMPSON_VAPOUR" or marker == "SURFACE_TENSION_SAT_LIQ" or marker == "TEMPERATURE" or marker == "PRESSURE":
        return True
    else:
        return False


def getNotice() -> str:
    return "# This file is autogenerated by the nistExporter (https://gitlab.com/0xhexdec/nistexporter)\n# For further documentation, code, feature requests and bugreports head over there.\n# If possible, keep this notice somewhere in this file.\n"


def nistIndexToIndex(index: int) -> int:
    if (index == 1):
        return 0
    elif (index == 2):
        return 1
    elif (index == 5):
        return 2
    elif (index == 8):
        return 3
    elif (index == 10):
        return 4
    elif (index == 11):
        return 5
    elif (index == 12):
        return 6


def indexToType(index: int) -> str:
    if (index == 0):
        return "pressure"
    elif (index == 1):
        return "desity"
    elif (index == 2):
        return "enthalpy"
    elif (index == 3):
        return "cp"
    elif (index == 4):
        return "jouleThompson"
    elif (index == 5):
        return "viscosity"
    elif (index == 6):
        return "conductivity"


if __name__ == "__main__":
    main()
